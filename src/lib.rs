#![recursion_limit="1024"]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

mod components;
mod external;
mod model;
mod util;
mod three_view;


use wasm_bindgen::prelude::*;
use yew::prelude::*;
use yew::services::ConsoleService;
use components::page::manager::ModelManager;
use web_sys::{Element, Document};
//use three_view::three_view::amain;

// Called when the wasm module is instantiated.
// Find the element we want to mount to, then mount this element to it.
// #[wasm_bindgen(start)]
// pub fn main() {
//     ConsoleService::info(&format!("Running {}", "Lib main"));
//     let window = web_sys::window().expect("no global `window` exists");
//     let document: Document = window.document().expect("should have a document on window");
//     let mounting_div: Element = document.get_element_by_id("model-manager").expect("Should have a model-manager id");
//     //App::<ModelManager>::new().mount(mounting_div);
//     amain();
//  }


// three_d run
include!("three_view/three_view.rs");
include!("main.rs");


use wasm_bindgen::prelude::*;

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue>
{
    console_log::init_with_level(log::Level::Debug).unwrap();

    use log::info;
    info!("Logging works!");



    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    bmain();
    amain();

    Ok(())
}
