
#[derive(Clone, serde_derive::Serialize, serde_derive::Deserialize, Debug)]
pub struct Detail {
    pub ownership: String,
    pub contact: String,
    #[serde(alias = "contact-email")]
    pub contact_email: String,
}

#[derive(Clone, serde_derive::Serialize, serde_derive::Deserialize, Debug)]
pub struct ModelSummary {
    pub uuid : String,
    pub name: String,
    pub summary: String,
    pub detail: Detail
}


impl ModelSummary {
    pub fn make_test_summary(id: i32) -> ModelSummary {
        ModelSummary{uuid: id.to_string(), 
                     name: format!("Dummy Model {}",id),
                     summary: format!("Test model summary for model {}", id), 
                     detail: Detail{ownership: "Dummy Owner".to_owned(),
                                    contact: "Dummy contact".to_owned(),
                                    contact_email: "Dummy email".to_owned()}}
    }
}

pub fn make_test_summaries(qty: i32) -> Vec::<ModelSummary>{
    (0..qty).map(|x| ModelSummary::make_test_summary(x)).collect()
}
