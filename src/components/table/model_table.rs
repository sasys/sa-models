use std::collections::HashMap;

use anyhow::Result;

use yew::format::{Json, Nothing};
use yew::prelude::*;
use yew::services::{ConsoleService, fetch::{FetchService, FetchTask}};
use yew::Properties;
use yew::agent::{Bridged, Dispatched, Dispatcher};

use yew_material::{MatButton, MatIconButton};

use http::{Request, Response};


use crate::components::bus::event_bus::EventBus;
use crate::components::bus::status_event_bus;
use crate::components::table::messages::Msg;
use crate::components::status::status_message;
use crate::model::summary::{make_test_summaries, Detail, ModelSummary};



static MODEL_SUMMARY: &str = "http://sa.softwarebynumbers.com/api/model/summary";

#[derive(Properties, Clone, Default)]
pub struct TableProps {
    pub model_summaries: Vec<ModelSummary>,
    pub ownership_states: HashMap<String, bool>,
    pub explore_states: HashMap<String, bool>,
    pub tag_states: HashMap<String, bool>,
    pub explore3: String
}

pub struct ModelTable {
    props: TableProps,
    link: ComponentLink<Self>,
    fetch_task: Option<FetchTask>,
    error: Option<String>,
    _producer: Box<dyn Bridge<EventBus>>,
    status_event_bus: Dispatcher<status_event_bus::StatusEventBus>,
}

pub fn toggle_column_state(states: &mut HashMap<String, bool>) {
    for val in states.values_mut() {
        *val = !*val;
    }
}

impl Component for ModelTable {
    type Properties = TableProps;
    type Message = Msg;

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        ConsoleService::info(&format!("Update: {:?}", "ModelTable create"));
        let request = Request::get(MODEL_SUMMARY)
            .body(Nothing)
            .expect("Could not build request.");

        let callback = link.callback(
            |response: Response<Json<Result<Vec<ModelSummary>, anyhow::Error>>>| {
                let Json(data) = response.into_body();
                Msg::UpdateModels(data)
            },
        );
        let task = FetchService::fetch(request, callback).expect("failed to start request");

        let callback = link.callback(Msg::UpdateSummaries);

        ModelTable {
            props,
            link,
            fetch_task: Some(task),
            error: None,
            _producer: EventBus::bridge(callback),
            status_event_bus: status_event_bus::StatusEventBus::dispatcher(),

        }
    }

    fn view(&self) -> Html {

        //Render an expand/contract symbol
        ConsoleService::info(&format!("Update: {:?}", "ModelTable view"));

        let render_expcon1 = |expand: bool, uuid: String| {
            if expand {
                html! {<div class="tooltip" onclick=self.link.callback(move |_| Msg::ContractOwnership(uuid.clone()))>
                     <span class="tooltiptext">{"Hide ownership"}</span>
                     <MatIconButton label="Tags" icon="expand_less"></MatIconButton>
                </div>}
            } else {
                html! {<div class="tooltip" onclick=self.link.callback(move |_| Msg::ExpandOwnership(uuid.clone()))>
                     <span class="tooltiptext">{"Show ownership"}</span>
                     <MatIconButton label="Tags" icon="expand_more"></MatIconButton>
                </div>}
            }
        };

        let render_explore3 = |uuid: String| {

            html! {<div class="tooltip">
                        <span class="tooltiptext">{"Disabled experimantal three d model explorer"}</span>
                        <MatIconButton label="Tags" icon="explore" disabled=true></MatIconButton>
                   </div>
            }
        };

        //Render the detail struct
        let render_details = |details: &Detail| {
            html! {
                <>
                    <li>{"Owner   : "} {&details.ownership}</li>
                    <li>{"Contact : "} {&details.contact}</li>
                    <li>{"Email   : "} {&details.contact_email}</li>
                </>
            }
        };

        //Render table rows as fragments
        let render_item = |item: &ModelSummary, ownership: bool, explore: bool, tag: bool| {
            let uuid1 = item.uuid.clone();
            let uuid2 = item.uuid.clone();
            html! {
                <>
                    <tr><td class="explorer-table-cell">{&item.name}</td>
                        <td class="explorer-table-cell">{&item.uuid}</td>
                        <td class="explorer-table-cell">{&item.summary}</td>
                        <td class="explorer-table-cell">
                            {if ownership {
                                html! {<ul>{render_details(&item.detail)}</ul>}}
                             else {
                                html! {
                                    <div onclick=self.link.callback(move |_| Msg::ExpandOwnership(uuid1.clone()))>
                                        <MatIconButton icon="more_horiz" ></MatIconButton>
                                    </div>
                                }
                             }
                            }
                        </td>
                        <td class="explorer-table-cell">
                            {if tag {
                                html! {
                                    <div onclick=self.link.callback(move |_| Msg::ContractTag(uuid2.clone()))>
                                        <MatIconButton icon="more_vert" ></MatIconButton>
                                    </div>
                                }
                            }
                            else {
                                html! {
                                    <div onclick=self.link.callback(move |_| Msg::ExpandTag(uuid2.clone()))>
                                        <MatIconButton icon="more_horiz" ></MatIconButton>
                                    </div>
                                }
                             }
                            }
                        </td>
                        <td class="explorer-table-cell">
                            {render_expcon1(ownership, item.uuid.clone())}
                            {render_explore3(item.uuid.clone())}
                        </td>
                    </tr>
                </>
            }
        };

        html! {
            <>
            <table class="explorer-table">
                <tr class="explorer-table-title-row">
                    <th class="explorer-table-title-cell">{"Name"}</th>
                    <th class="explorer-table-title-cell">{"ID"}</th>
                    <th class="explorer-table-title-cell">{"Summary"}</th>

                    <th class="explorer-table-tags-title-cell">
                        <div class="tooltip" onclick=self.link.callback(|_|  Msg::ToggleOwnership)>
                            <span class="tooltiptext">{"Open/Close ownership"}</span>
                            <MatButton label="Ownership" icon="unfold_more" dense=true></MatButton>
                        </div>
                    </th>
                    <th class="explorer-table-title-cell">
                        <div class="tooltip" onclick=self.link.callback(|_|  Msg::ToggleTags)>
                            <span class="tooltiptext">{"Open/Close tags"}</span>
                            <MatButton label="Tags" icon="unfold_more" dense=true></MatButton>
                        </div>
                    </th>
                    <th class="explorer-table-title-cell">{"Actions"}</th>
                </tr>

                //NOTE THE CURRYING HERE
                { for self.props.model_summaries.iter().map(move |x| render_item(x, self.props.ownership_states[&x.uuid], self.props.explore_states[&x.uuid], self.props.tag_states[&x.uuid]))}

            </table>
            </>
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        ConsoleService::info(&format!("Update ModelTable message: {:?}", msg));

        match msg {

            Msg::UpdateSummaries(_) => {
                ConsoleService::info(&format!(
                    "Update: {:?}",
                    "ModelTable update UpdateSummaries"
                ));
                let request = Request::get(MODEL_SUMMARY)
                    .body(Nothing)
                    .expect("Could not build request.");

                let callback = self.link.callback(
                    |response: Response<Json<Result<Vec<ModelSummary>, anyhow::Error>>>| {
                        let Json(data) = response.into_body();
                        Msg::UpdateModels(data)
                    },
                );

                let task = FetchService::fetch(request, callback).expect("failed to start request");
                self.fetch_task = Some(task);
                true
            }

            Msg::UpdateModels(models) => {
                ConsoleService::info(&format!("Update: {:?}", "ModelTable update UpdateModels"));

                match models {
                    Ok(summaries) => {
                        self.props.model_summaries = summaries;
                        self.props.ownership_states = HashMap::<String, bool>::new(); //Deallocate or clear?
                        self.props.explore_states = HashMap::<String, bool>::new(); //Deallocate or clear?
                        self.props.tag_states = HashMap::<String, bool>::new(); //Deallocate or clear?
                        for summary in &self.props.model_summaries {
                            self.props
                                .ownership_states
                                .insert(summary.uuid.clone(), false);
                            self.props
                                .explore_states
                                .insert(summary.uuid.clone(), false);
                        self.props.tag_states.insert(summary.uuid.clone(), false);
                        }
                        self.status_event_bus.send(status_message::Msg::Update("sa-store".to_owned()));
                    }

                    Err(error) => {

                        //RENDERING DUMMY DATA NEEDS A DEBUG SWITCH

                        self.error = Some(error.to_string());
                        self.props.model_summaries = make_test_summaries(200);
                        self.props.ownership_states = HashMap::<String, bool>::new(); //Deallocate or clear?
                        self.props.explore_states = HashMap::<String, bool>::new(); //Deallocate or clear?
                        self.props.tag_states = HashMap::<String, bool>::new(); //Deallocate or clear?
                        for summary in &self.props.model_summaries {
                            self.props
                                .ownership_states
                                .insert(summary.uuid.clone(), false);
                            self.props.tag_states.insert(summary.uuid.clone(), false);
                        }
                        self.status_event_bus.send(status_message::Msg::Update("dummy source".to_owned()));
                    }
                }
                self.fetch_task = None;
                true
            }

            Msg::ExpandOwnership(uuid) => {
                self.props.ownership_states.insert(uuid, true);
                true
            }

            Msg::ContractOwnership(uuid) => {
                self.props.ownership_states.insert(uuid, false);
                true
            }

            Msg::ExpandTag(uuid) => {
                self.props.tag_states.insert(uuid, true);
                true
            }

            Msg::ContractTag(uuid) => {
                self.props.tag_states.insert(uuid, false);
                true
            }

            Msg::ToggleOwnership => {
                toggle_column_state(&mut self.props.ownership_states);
                true
            }

            Msg::ToggleTags => {
                toggle_column_state(&mut self.props.tag_states);
                true
            }

            //Hide the table and show the 3d explorer, set the uuid on the div
            Msg::Hide(uuid) => {

                ConsoleService::info(&format!("Hiding the table,: {:?}", &uuid));
                self.props.explore3 = uuid.clone();
                let document = yew::utils::document();
                let can_div_res = document.get_element_by_id("explorer-div");
                let c = can_div_res.unwrap();
                let _r = c.set_attribute("style", "display: block");
                let _r = c.set_attribute("model-id", &uuid);
                let table_div = document.get_element_by_id("model_table");
                let _r = table_div.unwrap().set_attribute("style", "display: none");
                true
            }

        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        ConsoleService::info(&format!("Update: {:?}", "ModelTable change"));
        let request = Request::get(MODEL_SUMMARY)
            .body(Nothing)
            .expect("Could not build request.");

        let callback = self.link.callback(
            |response: Response<Json<Result<Vec<ModelSummary>, anyhow::Error>>>| {
                let Json(data) = response.into_body();
                Msg::UpdateModels(data)
            },
        );
        let task = FetchService::fetch(request, callback).expect("failed to start request");
        self.fetch_task = Some(task);
        true
    }
}
