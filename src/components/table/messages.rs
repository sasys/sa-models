use crate::model::summary::{ModelSummary};


#[derive(Debug)]
pub enum Msg {

    UpdateSummaries(String), //Fetch model summaris from sources

    UpdateModels(Result<Vec<ModelSummary> ,anyhow::Error>),  //Update the model summaries on the screen

    ExpandOwnership(String), //Expand the tags for a specific summary

    ContractOwnership(String), //Contract the tags for a specific summary

    ExpandTag(String), //Expand the tags for a specific summary

    ContractTag(String), //Contract the tags for a specific summary

    ToggleTags,

    ToggleOwnership,

    Hide(String), //Hide/Show the table, probably because another view is up

}
