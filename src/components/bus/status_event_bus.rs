
use std::collections::HashSet;
use yew::worker::*;
use yew::services::ConsoleService;
use crate::components::status::status_message;

pub struct StatusEventBus {
    link: AgentLink<StatusEventBus>,
    subscribers: HashSet<HandlerId>,
}

impl Agent for StatusEventBus {
    type Reach = Context<Self>;
    type Message = ();
    type Input = status_message::Msg;
    type Output = status_message::Msg;

    fn create(link: AgentLink<Self>) -> Self {
        ConsoleService::info(&format!("{:?}", "Status Event Bus create"));
        Self {
            link,
            subscribers: HashSet::new(),
        }
    }

    fn update(&mut self, _msg: Self::Message) {}

    fn handle_input(&mut self, msg: Self::Input, id: HandlerId) {
        ConsoleService::info(&format!("Status Event Bus handle_input {:?} id; {:?}", msg, id));
        match msg {
            status_message::Msg::Update(s) => {
                for sub in self.subscribers.iter() {
                    ConsoleService::info(&format!("Status Event Bus Sub {:?}", sub));
                    if sub.is_respondable() {
                        self.link.respond(*sub, status_message::Msg::Update(s.clone()));
                    }
                }
            }
        }
    }

    fn connected(&mut self, id: HandlerId) {
        ConsoleService::info(&format!("Status Event Bus connected id {:?}", id));
        self.subscribers.insert(id);
    }

    fn disconnected(&mut self, id: HandlerId) {
        ConsoleService::info(&format!("Status Event Bus disconnected {:?}", id));
        self.subscribers.remove(&id);
    }
}