use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use yew::worker::*;
use yew::services::ConsoleService;


#[derive(Serialize, Deserialize, Debug)]
pub enum Request {
    EventBusMsg(String),
}

pub struct EventBus {
    link: AgentLink<EventBus>,
    subscribers: HashSet<HandlerId>,
}

impl Agent for EventBus {
    type Reach = Context<Self>;
    type Message = ();
    type Input = Request;
    type Output = String;

    fn create(link: AgentLink<Self>) -> Self {
        ConsoleService::info(&format!("{:?}", "EventBus create"));
        Self {
            link,
            subscribers: HashSet::new(),
        }
    }

    fn update(&mut self, _msg: Self::Message) {}

    fn handle_input(&mut self, msg: Self::Input, id: HandlerId) {
        ConsoleService::info(&format!("EventBus handle_input {:?} id; {:?}", msg, id));
        match msg {
            Request::EventBusMsg(s) => {
                for sub in self.subscribers.iter() {
                    ConsoleService::info(&format!("Event Bus Sub {:?}", sub));
                    if sub.is_respondable() {
                        self.link.respond(*sub, s.clone());
                    }
                }
            }
        }
    }

    fn connected(&mut self, id: HandlerId) {
        ConsoleService::info(&format!("EventBus connected id {:?}", id));
        self.subscribers.insert(id);
    }

    fn disconnected(&mut self, id: HandlerId) {
        ConsoleService::info(&format!("EventBus disconnected {:?}", id));
        self.subscribers.remove(&id);
    }
}