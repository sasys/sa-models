use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use yew::worker::*;
use yew::services::ConsoleService;


#[derive(Serialize, Deserialize, Debug)]
pub struct RefreshRequest {

}

#[derive(Serialize, Deserialize, Debug)]
pub struct RefreshResponse {

}

pub trait BusRequest: std::fmt::Debug {
    fn get_response(&self) -> Box<dyn BusResponse>;
}

pub trait BusResponse: std::fmt::Debug {

}

impl BusRequest for RefreshRequest {
    fn get_response(&self) -> Box<dyn BusResponse> {
        Box::new(RefreshResponse{})
    }
}

impl BusResponse for RefreshResponse {

}


// #[derive(Serialize, Deserialize, Debug)]
// pub enum Request {
//     EventBusMsg(String),
//     NewBusMsg(dyn BusMsg)
// }
//
// #[derive(Serialize, Deserialize, Debug)]
// pub enum Response {
//     NewBusMsg(BusMsg)
// }



pub struct EventBus {
    link: AgentLink<EventBus>,
    subscribers: HashSet<HandlerId>,
}

impl Agent for EventBus {
    type Reach = Context<Self>;
    type Message = ();
    type Input =  Box<dyn BusRequest>;
    type Output = Box<dyn BusResponse>;

    fn create(link: AgentLink<Self>) -> Self {
        ConsoleService::info(&format!("{:?}", "EventBus create"));
        Self {
            link,
            subscribers: HashSet::new(),
        }
    }

    fn update(&mut self, _msg: Self::Message) {}

    fn handle_input(&mut self, msg: Self::Input, id: HandlerId) {
        ConsoleService::info(&format!("EventBus handle_input {:?} id; {:?}", msg, id));
        match &msg {
            RefreshRequest => {
                for sub in self.subscribers.iter() {
                    ConsoleService::info(&format!("Event Bus Sub {:?}", sub));
                    if sub.is_respondable() {
                        self.link.respond(*sub, msg.get_response());
                    }
                }
            },
        }
    }

    fn connected(&mut self, id: HandlerId) {
        ConsoleService::info(&format!("EventBus connected id {:?}", id));
        self.subscribers.insert(id);
    }

    fn disconnected(&mut self, id: HandlerId) {
        ConsoleService::info(&format!("EventBus disconnected {:?}", id));
        self.subscribers.remove(&id);
    }
}
