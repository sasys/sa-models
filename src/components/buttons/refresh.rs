 
use crate::components::bus::event_bus;
use yew_material::MatIconButton;
use yew::agent::{Dispatched, Dispatcher};
use yew::prelude::*;
use yew::services::ConsoleService;

pub enum Msg {
    Clicked,
}

#[derive(Properties, Clone, Default)]
pub struct Props {
    message: String,
}

pub struct RefreshButton {
    link: ComponentLink<RefreshButton>,
    event_bus: Dispatcher<event_bus::EventBus>,
}

impl Component for RefreshButton {
    type Message = Msg;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        ConsoleService::info(&format!("{:?}", "RefreshButton create"));
        Self {
            link,
            event_bus: event_bus::EventBus::dispatcher(),
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        ConsoleService::info(&format!("{:?}", "RefreshButton update"));
        match msg {
            Msg::Clicked => {
                self.event_bus
                    .send(event_bus::Request::EventBusMsg("UpdateModels".to_owned()));
                false
            }
        }
    }

    fn view(&self) -> Html {
        html! {
            <div onclick=self.link.callback(|_|  Msg::Clicked)>
                <MatIconButton icon="refresh"></MatIconButton>
            </div>
        }
    }
} 