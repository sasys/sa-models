
use crate::components::bus::event_bus;
use yew_material::MatIconButton;
use yew::agent::{Dispatched, Dispatcher};
use yew::prelude::*;
use yew::services::ConsoleService;

pub enum Msg {
    Clicked,
}
                        //<div onclick=self.link.callback(move |_| Msg::Hide(String::from("A")))>  //Dont need the parameter
#[derive(Properties, Clone, Default)]
pub struct Props {
    message: String,
}

pub struct HomeButton {
    link: ComponentLink<HomeButton>,
    event_bus: Dispatcher<event_bus::EventBus>,
}

impl Component for HomeButton {
    type Message = Msg;
    type Properties = ();

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        ConsoleService::info(&format!("{:?}", "HomeButton create"));
        Self {
            link,
            event_bus: event_bus::EventBus::dispatcher(),
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        ConsoleService::info(&format!("{:?}", "HomeButton update"));
        match msg {
            Msg::Clicked => {
                ConsoleService::info(&format!("HomeButton Msg:Clicked"));
                let document = yew::utils::document();
                let can_div_res = document.get_element_by_id("explorer-div");
                let c = can_div_res.unwrap();
                let _r = c.set_attribute("style", "display: none");
                let table_div = document.get_element_by_id("model_table");
                let _r = table_div.unwrap().set_attribute("style", "display: block");
                self.event_bus
                    .send(event_bus::Request::EventBusMsg("Show".to_owned()));
                false
            }
        }
    }

    fn view(&self) -> Html {
        html! {
            <div onclick=self.link.callback(|_|  Msg::Clicked)>
                <MatIconButton icon="home"></MatIconButton>
            </div>
        }
    }
}
