
use crate::components::bus::status_event_bus::{StatusEventBus};
use yew::prelude::*;
use yew::services::ConsoleService;

#[derive(serde_derive::Serialize, serde_derive::Deserialize, Debug)]
pub enum Msg {
    Update(String),  //where string is the source
}

#[derive(Properties, Clone, Default)]
pub struct StatusProps {
    pub status_text: String,
    pub title_text: String,
}

pub struct Status {
    props: StatusProps,
    link: ComponentLink<Status>,
    _producer: Box<dyn Bridge<StatusEventBus>>
}
 
impl Component for Status {

    type Message = Msg;
    type Properties = StatusProps;

    fn create(properties: Self::Properties, link: ComponentLink<Self>) -> Self {
        ConsoleService::info(&format!("{:?}", "RefreshButton create"));
        let callback: Callback<Msg> = link.callback(|m: Msg|m);
        Self {
            props: properties, //StatusProps{status_text: String::from(""), title_text: String::from("")},
            link,
            _producer: StatusEventBus::bridge(callback),
        }
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
 
        ConsoleService::info(&format!("Update: {:?}", "ModelTable view"));
        html! {
            <>
            {&self.props.title_text} {"(data from : "} {&self.props.status_text} {")"}
            </>
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        ConsoleService::info(&format!("Update status message: {:?}", msg));

        match msg {
            Msg::Update(text) => {
                self.props.status_text = text
            }
        }
        true
    }

}