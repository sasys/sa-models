
use yew::prelude::*;
use std::collections::HashMap;
use yew::services::ConsoleService;
use yew_material::top_app_bar_fixed::{MatTopAppBarTitle, MatTopAppBarActionItems, MatTopAppBarNavigationIcon};
use yew_material::top_app_bar_fixed::MatTopAppBarFixed;
use yew_material::icon_button::MatIconButton;

use crate::components::table::model_table::{TableProps, ModelTable};
use crate::components::buttons::refresh::RefreshButton;
use crate::components::buttons::home::HomeButton;
use crate::components::status::status_message::{Status, StatusProps};


pub struct ModelManager {
    link: ComponentLink<Self>,
}

pub enum Msg {

}

#[derive(Properties, Clone)]
struct Props {
}

impl Default for Props {
    fn default() -> Props {
        Props{}
    }
}

impl Component for ModelManager {

    type Message = Msg;
    type Properties = TableProps;

    fn create(_props: Self::Properties, link: ComponentLink<Self>) -> Self {
        ConsoleService::info(&format!("Update: {:?}", "ModelManager create"));
        ModelManager {link}
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        ConsoleService::info(&format!("Update: {:?}", "ModelManager change"));
        true
    }

    fn view(&self) -> Html {
        ConsoleService::info(&format!("Update: {:?}", "Model Manager view"));
        let table_props = TableProps{model_summaries: vec!(),
                                     ownership_states: HashMap::<String, bool>::new(),
                                     explore_states: HashMap::<String, bool>::new(),
                                     tag_states: HashMap::<String, bool>::new(),
                                     explore3: String::new()};
        let status_props: StatusProps = StatusProps{status_text: String::from(""), title_text: String::from("Model Manager - Teams Edition")};

        html! {
            <div>
                <div>
                    <MatTopAppBarFixed center_title=true>
                        <MatTopAppBarNavigationIcon>
                            <HomeButton></HomeButton>
                        </MatTopAppBarNavigationIcon>

                        <MatTopAppBarTitle>
                            <Status with status_props></Status>
                        </MatTopAppBarTitle>


                        <MatTopAppBarActionItems>
                            <RefreshButton></RefreshButton>
                        </MatTopAppBarActionItems>

                    </MatTopAppBarFixed>

                    <div id="model_table" class="explorer-div" style="display: block">
                        <ModelTable with table_props></ModelTable>
                    </div>
                </div>
                <div id= "explorer-div" style="display: none">
                    <canvas id="canvas" style="position: absolute;top:0;bottom: 0;left: 0;right: 0;margin:auto;"></canvas>
                </div>
            </div>

        }
    }
}
