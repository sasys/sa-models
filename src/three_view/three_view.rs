

use three_d::*;
use log::info;
use three_d::{Screen, window::Window};


pub fn amain() {
    let args: Vec<String> = std::env::args().collect();

    // Create a window (a canvas on web)
    let window = Window::new("Triangle", Some((1000,1000))).unwrap();

    // Get the graphics context from the window
    let context = window.gl();

    // Create a camera
    let mut camera = Camera::new_perspective(
        &context,
        vec3(0.0, 0.0, 2.0),
        vec3(0.0, 0.0, 0.0),
        vec3(0.0, 1.0, 0.0),
        degrees(45.0),
        window.viewport().aspect(),
        0.1,
        10.0,
    )
    .unwrap();

    // Create a CPU-side mesh consisting of a single colored square
    let cube_positions: Vec<f32> = vec![
         0.5, -0.5, 0.0,   // first triangle
        -0.5, -0.5, 0.0,
        -0.5,  0.5, 0.0,
         0.5, -0.5, 0.0,   //close first triangle {


         0.5,  0.5, 0.0,   //second triangle
        -0.5,  0.5, 0.0,
         0.5, -0.5, 0.0,   // close second triangle

         0.5, -0.5, -1.0,  //third triangle
        -0.5, -0.5, -1.0,
        -0.5,  0.5, -1.0,
         0.5, -0.5, -1.0,  //close third triangle

        -0.5, -0.5, -1.0,  //fourth triangle
        -0.5,  0.5, -1.0,
         0.5,  0.5, -1.0,
         0.5, -0.5, -1.0,  //close fourth triangle

         0.5, -0.5,  0.0,  //fith triangle
        -0.5, -0.5,  0.0,
         0.5, -0.5, -1.0,  //close fith triangle

        -0.5, -0.5, -1.0,  //sixth triangle
        -0.5, -0.5,  0.0,
         0.5, -0.5, -1.0,  //close sixth triangle


    ];



    //gray for a square
    let gray_square: Vec<u8> = vec![
        196, 192, 181, 150, // bottom right
        196, 192, 181, 150, // bottom left
        196, 192, 181, 150, // top
        196, 192, 181, 150,
        ]; // top

    let gray_cube: Vec<u8> = vec![
        196, 192, 181, 150,
        196, 192, 181, 150,
        196, 192, 181, 150,
        196, 192, 181, 150,
        196, 192, 181, 150,
        196, 192, 181, 150,
        196, 192, 181, 150,

        196, 192, 0, 150,
        196, 192, 0, 150,
        196, 192, 0, 150,
        196, 192, 0, 150,
        196, 192, 0, 150,
        196, 192, 0, 150,
        196, 192, 0, 150,
        196, 192, 0, 150,

        196, 100, 0, 150,
        196, 100, 0, 150,
        196, 100, 0, 150,
        196, 100, 0, 150,
        196, 100, 0, 150,
        196, 100, 0, 150,
    ];

    let cpu_cube_mesh = CPUMesh {
        positions: cube_positions,
        colors: Some(gray_cube),
        ..Default::default()
    };




    let mut cpu_square = CPUMesh::square(0.5);
    //cpu_sphere.compute_normals();
    cpu_square.colors = Some(gray_square);

    let vx: cgmath::Vector4<f32> = cgmath::Vector4::<f32>{x: 0.5, y: 0.0, z: 0.0, w: 0.0};
    let vy: cgmath::Vector4<f32> = cgmath::Vector4::<f32>{x: 0.0, y: 0.5, z: 0.0, w: 0.0};
    let vz: cgmath::Vector4<f32> = cgmath::Vector4::<f32>{x: 0.0, y: 0.0, z: 0.5, w: 0.0};
    let vw: cgmath::Vector4<f32> = cgmath::Vector4::<f32>{x: 0.0, y: 0.0, z: 0.0, w: 1.0};


    let t1: cgmath::Matrix4<f32> = cgmath::Matrix4{x: vx,
                                                   y: vy,
                                                   z: vz,
                                                   w: vw};



    let vp = Viewport::new_at_origo(200, 200);

    // Construct a mesh, thereby transferring the mesh data to the GPU
    let mut mesh = Mesh::new(&context, &cpu_cube_mesh).unwrap();

    // Start the main render loop
    window.render_loop(move |frame_input: FrameInput| // Begin a new frame with an updated frame input
    {
        // Ensure the aspect ratio of the camera matches the aspect ratio of the window viewport
        camera.set_aspect(frame_input.viewport.aspect()).unwrap();

        let posn = vec3(2.0, 2.0, 2.0);
        let target = vec3(0.0, 0.0, 0.0);
        let up = vec3(0.0, 1.0, 0.0);

        let _r = camera.set_view(posn, target, up);

        // Start writing to the screen and clears the color and depth
        Screen::write(&context, &ClearState::color_and_depth(0.8, 0.8, 0.8, 1.0, 1.0), || {
            // Set the current transformation of the triangle
            //mesh.transformation = Mat4::from_angle_y(radians((frame_input.accumulated_time * 0.005) as f32));
            let t = Mat4::from_angle_y(radians((frame_input.accumulated_time * 0.005) as f32));

            //let t = Mat4::from_angle_y(radians(0 as f32));

            // Render the triangle with the per vertex colors defined at construction
            mesh.render_color(RenderStates::default(), frame_input.viewport, &t, &camera)?;
            let axes_plus = Axes::new(&context,0.02 ,1.5);
            let axes_minus = Axes::new(&context,0.02 ,-1.5);
            axes_plus.unwrap().render(frame_input.viewport, &t1, &camera)?;
            axes_minus.unwrap().render(frame_input.viewport, &t1, &camera)?;
            Ok(())
        }).unwrap();




        if args.len() > 1 {
            // To automatically generate screenshots of the examples, can safely be ignored.
            FrameOutput {screenshot: Some(args[1].clone().into()), exit: true, ..Default::default()}
        } else {
            // Returns default frame output to end the frame
            FrameOutput::default()
        }
    }).unwrap();
}
