// #![recursion_limit="1024"]
// #[global_allocator]
//
// static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

//mod components;
// mod model;
// mod util;
// mod three_view;


// use yew::prelude::*;
// use yew::services::ConsoleService;
use components as main_components;


pub fn bmain() {
    yew::services::ConsoleService::info(&format!("Running {}", "Main main"));
    App::<main_components::page::manager::ModelManager>::new().mount_to_body();

}
