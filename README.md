


# Model Manager Prototype

[![CircleCI](https://circleci.com/bb/sasys/sa-models.svg?style=svg)](https://circleci.com/bb/sasys/sa-models)
[![Netlify Status](https://api.netlify.com/api/v1/badges/650d2333-da25-4ed3-9087-a17397c35e2e/deploy-status)](https://sa-models.demo.softwarebynumbers.com/)

A test of Rust and WASM using trunk.  An experiment to add a three dimensional
view of a model.  

Displays the stored models in a table with links to edit meta data, tags etc and open the model for editing.  

A button is provided that will eventually bring up the three d view of a model using the rust three_d crate,
with the home button on the tool bar bringing back the tabular view.  Currently experimental.


![Table model view](resources/images/table-view.png)



## Development Build

```
trunk serve --release --port 9001
```

## Optimised Deployment Build


NOT cargo build --profile release -Z unstable-options

```
trunk build --release
```

Run a server as

```
http-server dist --port 9001
```

Then browser to localhost:9001

The wasm file is then 443KB ie its got bigger.

## Build With wasmpack

See https://yew.rs/docs/en/next/getting-started/project-setup/using-wasm-pack

This gives a non-trunk build as a stepping stone to a component.  Note that I had to duplicate main into lib in the short term.  This is an ongoing experiment so hey.

To get a clean build do __ALL__ of the following:
```
cargo clean
# Remove the contents of pkg dir
wasm-pack build --target web
http-server -p9001

```
Then browse to http://localhost:9001/zz.html


## Netlify Local cli Build

```
circleci config process .circleci/config.yml
circleci local execute --job community-build
```

## Docker Build And Deploy

##
